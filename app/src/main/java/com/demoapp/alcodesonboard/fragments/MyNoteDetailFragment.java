package com.demoapp.alcodesonboard.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;

import com.afollestad.materialdialogs.MaterialDialog;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.MyNoteDetailActivity;
import com.demoapp.alcodesonboard.adapters.MyNotesAdapter;
import com.demoapp.alcodesonboard.database.entities.MyNote;
import com.demoapp.alcodesonboard.repositories.MyNotesRepository;
import com.demoapp.alcodesonboard.utils.DatabaseHelper;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MyNoteDetailFragment extends Fragment {

    public static final String TAG = MyNoteDetailFragment.class.getSimpleName();

    private static final String ARG_LONG_MY_NOTE_ID = "ARG_LONG_MY_NOTE_ID";

    @BindView(R.id.edittext_title)
    protected TextInputEditText mEditTextTitle;

    @BindView(R.id.edittext_content)
    protected TextInputEditText mEditTextContent;

    private Unbinder mUnbinder;
    private Long mMyNoteId = 0L;

    public MyNoteDetailFragment() {
    }

    public static MyNoteDetailFragment newInstance(long id) {
        Bundle args = new Bundle();
        args.putLong(ARG_LONG_MY_NOTE_ID, id);

        MyNoteDetailFragment fragment = new MyNoteDetailFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_note_detail, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Check arguments pass from previous page.
        Bundle args = getArguments();

        if (args != null) {
            mMyNoteId = args.getLong(ARG_LONG_MY_NOTE_ID, 0);
        }

        initView();
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_my_note_detail, menu);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);

        // TODO change menu label "Save" to "Create" for new note.
        MenuItem menuItem = menu.findItem(R.id.menu_save);
        String title = mEditTextTitle.getText().toString();

        if(title.isEmpty())  {
            menuItem.setTitle("Create");
        }
        else {
            menuItem.setTitle("Save");
        }




    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.menu_save) {
            // TODO show confirm dialog before continue.
            new MaterialDialog.Builder(super.getContext())
                    .title("Confirm Operation")
                    .content("Are you sure to continue?")
                    .positiveText("Yes")
                    .onPositive((dialog, which) -> {

                        // TODO check email and password is blank or not.
                        String title = mEditTextTitle.getText().toString();
                        String content = mEditTextContent.getText().toString();

                        if(title.isEmpty() || content.isEmpty()) {
                            if(title.isEmpty()) {
                                Toast.makeText((getActivity()), "Please give a title name.", Toast.LENGTH_LONG).show();
                            }
                            else {
                                Toast.makeText((getActivity()), "Please enter your content.", Toast.LENGTH_LONG).show();
                            }
                        }
                        else {

                            // Save record and return to list.
                            MyNote myNote = new MyNote();
                            myNote.setTitle(title);
                            myNote.setContent(content);

                            // TODO BAD practice, should move Database operations to Repository.
                            if (mMyNoteId > 0) {
                                // Update record.
                                MyNotesRepository.getInstance().editNote(getActivity(), mMyNoteId, title, content);
                                Toast.makeText(getActivity(), "Note saved.", Toast.LENGTH_SHORT).show();

/*                                myNote.setId(mMyNoteId);
                                DatabaseHelper.getInstance(getActivity())
                                        .getMyNoteDao()
                                        .save(myNote);*/
                            } else {
                                // Create record.
                                MyNotesRepository.getInstance().addNote(getActivity(), title, content);
                                Toast.makeText(getActivity(), "Note created.", Toast.LENGTH_SHORT).show();

/*                                DatabaseHelper.getInstance(getActivity())
                                        .getMyNoteDao()
                                        .insert(myNote);*/
                            }



                            getActivity().setResult(MyNoteDetailActivity.RESULT_CONTENT_MODIFIED);
                            getActivity().finish();
                        }
                    })
                    .negativeText("No")
                    .onNegative(null)
                    .show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initView() {
        // TODO BAD practice, should move Database operations to Repository.
        MyNotesRepository data = new MyNotesRepository();
        if (mMyNoteId > 0) {
            if (data != null) {
                mEditTextTitle.setText(data.getTitle(getContext(), mMyNoteId));
                mEditTextContent.setText(data.getContent(getContext(), mMyNoteId));
            } else {
                // Record not found.
                Toast.makeText(getActivity(), "Note not found.", Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
        }
    }
}
